using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : Entity
{
    private Animator anim;
    private float clock;
    private PlayerInput PlayerInput;
    private InputAction movement;
    new void Start()
    {
        base.Start();
        PlayerInput = GetComponent<PlayerInput>();
        movement = PlayerInput.actions.FindAction("Movement");
        anim = GetComponent<Animator>();
        clock = Time.time;
    }
    void Update()
    {
        transform.position += speed * Time.deltaTime * new Vector3(movement.ReadValue<Vector2>().x, 0, movement.ReadValue<Vector2>().y);

        if (Time.time - clock > 0.1f)
        {
            if (anim.GetFloat("Attacking") > 0) anim.SetFloat("Attacking", anim.GetFloat("Attacking") - 1);
            clock = Time.time;
        }

        anim.SetBool("Dance", Input.GetKey(KeyCode.B));

        anim.SetBool("Walking", movement.ReadValue<Vector2>() != Vector2.zero);
        /*
        if (Input.GetKeyDown(KeyCode.Escape)) anim.SetBool("Death", true);
        if (Input.GetKeyDown(KeyCode.Mouse0) && anim.GetFloat("Attacking") == 0) anim.SetFloat("Attacking", 17);

        if (Input.GetKeyDown(KeyCode.Space)) GetComponent<Rigidbody>().velocity = new Vector3(0,10,0);
        */
    }
    /*
    public void Movement(InputAction.CallbackContext ctx)
    {
        Debug.Log("a");
        movement = ctx.ReadValue<Vector2>() * speed;
    }*/
}
