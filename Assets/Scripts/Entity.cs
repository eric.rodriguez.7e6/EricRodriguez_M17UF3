using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody rb;
    public float speed;
    public void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
}
